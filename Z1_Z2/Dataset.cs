﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Dataset
    {
        private List<List<double>> data;
        public Dataset()
        {
            this.data = new List<List<double>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            string[] lines = System.IO.File.ReadAllLines(filePath);
            foreach (string line in lines)
            {
                List<double> row = new List<double>(Array.ConvertAll(line.Split(','), double.Parse));
                this.data.Add(row);
            }
        }
        public IList<List<double>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<double>>(this.data);
        }
        //public void PrintData() //ToString je bolja opcija
        //{
        //    foreach (List<double> list in this.data)
        //    {
        //        foreach (double d in list)
        //        {
        //            Console.Write(d + ",\t");
        //        }
        //        Console.WriteLine("\n");
        //    }
        //}
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (List<double> list in this.data)
            {
                foreach (double d in list)
                {
                    stringBuilder.Append(d+", ");
                }
                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();
        }
    }
}
