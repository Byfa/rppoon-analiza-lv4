﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            Dataset dataset = new Dataset("primjer.csv");

            Console.WriteLine(dataset.ToString());
            Analyzer3rdParty service = new Analyzer3rdParty();
            Adapter adapter = new Adapter(service);

            adapter.CalculateAveragePerRow(dataset);
            adapter.CalculateAveragePerColumn(dataset);

            Console.ReadKey();
        }
    }
}
