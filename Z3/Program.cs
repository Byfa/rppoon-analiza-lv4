﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter Renter = new RentingConsolePrinter();
            List<IRentable> ToRent = new List<IRentable>();
            Book bookToRent = new Book("Overlord");
            Video videoToRent = new Video("Monty Python and the Holy Grail");
            ToRent.Add(bookToRent);
            ToRent.Add(videoToRent);


            Renter.PrintTotalPrice(ToRent);
            Renter.DisplayItems(ToRent);

            Console.ReadKey();
        }
    }
}
