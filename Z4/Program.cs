﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter Renter = new RentingConsolePrinter();
            List<IRentable> ToRent = new List<IRentable>();
            Book bookToRent = new Book("Overlord");
            Video videoToRent = new Video("Monty Python and the Holy Grail");
            ToRent.Add(bookToRent);
            ToRent.Add(videoToRent);
            
            Renter.PrintTotalPrice(ToRent);
            Renter.DisplayItems(ToRent);

            Book HotBookToRent = new Book("Mo Dao Zu Shi");
            Video HotVideoToRent = new Video("Untamed");
            HotItem HotNewBook = new HotItem(HotBookToRent);
            HotItem HotNewVideo = new HotItem(HotVideoToRent);

            ToRent.Add(HotNewBook);
            ToRent.Add(HotNewVideo);

            Renter.PrintTotalPrice(ToRent);
            Renter.DisplayItems(ToRent);

            /*
                Cijena rentanja se povecala za 1.99 za item koji je oznacen sa HOT,
                osim toga ispred ispisa njhovih imena pise "Trending:".
                Razlika je u tome da smo napravili novi objekt koji samo je 
                nastao tako da smo prekrili stari s novim informacijama (promjenili klasu)
            */
        

            Console.ReadKey();
        }
    }
}
