﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            RentingConsolePrinter Renter = new RentingConsolePrinter();
            List<IRentable> ToRent = new List<IRentable>();
            Book bookToRent = new Book("Overlord");
            Video videoToRent = new Video("Monty Python and the Holy Grail");
            ToRent.Add(bookToRent);
            ToRent.Add(videoToRent);

            Console.WriteLine("ZADATAK 3:");
            Renter.PrintTotalPrice(ToRent);
            Renter.DisplayItems(ToRent);

            Book HotBookToRent = new Book("Mo Dao Zu Shi");
            Video HotVideoToRent = new Video("Untamed");
            HotItem HotNewBook = new HotItem(HotBookToRent);
            HotItem HotNewVideo = new HotItem(HotVideoToRent);

            Console.WriteLine("ZADATAK 4:");
            ToRent.Add(HotNewBook);
            ToRent.Add(HotNewVideo);

            Renter.PrintTotalPrice(ToRent);
            Renter.DisplayItems(ToRent);

            List<IRentable> flashSale = new List<IRentable>();
            DiscountedItem discountedBook = new DiscountedItem(bookToRent,10);
            DiscountedItem discountedHotBook = new DiscountedItem(HotNewBook,20);
            DiscountedItem discountedVideo = new DiscountedItem(videoToRent,25);
            DiscountedItem discountedHotVideo = new DiscountedItem(HotNewVideo,15);

            flashSale.Add(discountedBook);
            flashSale.Add(discountedHotBook);
            flashSale.Add(discountedVideo);
            flashSale.Add(discountedHotVideo);

            Console.WriteLine("ZADATAK 5:");
            Renter.PrintTotalPrice(flashSale);
            Renter.DisplayItems(flashSale);

            Console.ReadKey();
        }
    }
}
