﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailValidator emailValidator = new EmailValidator(8);
            List<string> emailList = new List<string>();

            string test1 = "mirko.vlasic.com";
            string test2 = "mali@mala.eh";
            string test3 = "mala@mail.com";
            string test4 = "mala@kupka.hr";
            string test5 = "a@a.com";

            emailList.Add(test1);
            emailList.Add(test2);
            emailList.Add(test3);
            emailList.Add(test4);
            emailList.Add(test5);

            foreach (string email in emailList)
            {
                Console.WriteLine(emailValidator.IsValidAddress(email));
            }
            Console.ReadKey();
        }
    }
}
