﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            EmailValidator emailValidator = new EmailValidator(8);
            PasswordValidator passwordValidator = new PasswordValidator(8);
            RegistrationValidator registrationValidator = new RegistrationValidator(passwordValidator, emailValidator);
            while (!registrationValidator.IsUserEntryValid(UserEntry.ReadUserFromConsole()))
            {
                UserEntry.ReadUserFromConsole();
            }
 
            Console.ReadKey();
        }
    }
}
