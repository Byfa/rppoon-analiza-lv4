﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD6
{
    class RegistrationValidator : IRegistrationValidator
    {
        private PasswordValidator passwordValidator;
        private EmailValidator emailValidator;
        public RegistrationValidator(PasswordValidator password, EmailValidator email)
        {
            this.passwordValidator = password;
            this.emailValidator = email;
        }
        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password);
        }
    }
}
